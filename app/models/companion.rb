class Companion < ActiveRecord::Base
  has_attached_file :avatar, :styles => { :medium => "180x180>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  has_and_belongs_to_many :extras
end
