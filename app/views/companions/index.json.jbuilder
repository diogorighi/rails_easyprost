json.array!(@companions) do |companion|
  json.extract! companion, :id, :gender, :age, :price, :boobsize
  json.url companion_url(companion, format: :json)
end
