json.array!(@extras) do |extra|
  json.extract! extra, :id, :name
  json.url extra_url(extra, format: :json)
end
