class CreateCompanions < ActiveRecord::Migration
  def change
    create_table :companions do |t|
      t.string :gender
      t.integer :age
      t.decimal :price
      t.string :boobsize

      t.timestamps
    end
  end
end
