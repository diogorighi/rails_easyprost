class AddNameToCompanions < ActiveRecord::Migration
  def change
    add_column :companions, :name, :string
  end
end
