class CreateCompanionsExtras < ActiveRecord::Migration
  def change
    create_table :companions_extras, :id => false do |t|
      t.integer :companion_id
      t.integer :extra_id
    end
  end
end
