class AddAttachmentAvatarToCompanions < ActiveRecord::Migration
  def self.up
    change_table :companions do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :companions, :avatar
  end
end
