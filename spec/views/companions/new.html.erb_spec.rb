require 'spec_helper'

describe "companions/new" do
  before(:each) do
    assign(:companion, stub_model(Companion,
      :gender => "MyString",
      :age => 1,
      :price => "9.99",
      :boobsize => "MyString"
    ).as_new_record)
  end

  it "renders new companion form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", companions_path, "post" do
      assert_select "input#companion_gender[name=?]", "companion[gender]"
      assert_select "input#companion_age[name=?]", "companion[age]"
      assert_select "input#companion_price[name=?]", "companion[price]"
      assert_select "input#companion_boobsize[name=?]", "companion[boobsize]"
    end
  end
end
