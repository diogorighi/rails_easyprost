require 'spec_helper'

describe "companions/index" do
  before(:each) do
    assign(:companions, [
      stub_model(Companion,
        :gender => "Gender",
        :age => 1,
        :price => "9.99",
        :boobsize => "Boobsize"
      ),
      stub_model(Companion,
        :gender => "Gender",
        :age => 1,
        :price => "9.99",
        :boobsize => "Boobsize"
      )
    ])
  end

  it "renders a list of companions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Gender".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Boobsize".to_s, :count => 2
  end
end
