require 'spec_helper'

describe "companions/show" do
  before(:each) do
    @companion = assign(:companion, stub_model(Companion,
      :gender => "Gender",
      :age => 1,
      :price => "9.99",
      :boobsize => "Boobsize"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Gender/)
    rendered.should match(/1/)
    rendered.should match(/9.99/)
    rendered.should match(/Boobsize/)
  end
end
