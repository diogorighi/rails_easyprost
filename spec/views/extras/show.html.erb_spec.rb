require 'spec_helper'

describe "extras/show" do
  before(:each) do
    @extra = assign(:extra, stub_model(Extra,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
