require "spec_helper"

describe CompanionsController do
  describe "routing" do

    it "routes to #index" do
      get("/companions").should route_to("companions#index")
    end

    it "routes to #new" do
      get("/companions/new").should route_to("companions#new")
    end

    it "routes to #show" do
      get("/companions/1").should route_to("companions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/companions/1/edit").should route_to("companions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/companions").should route_to("companions#create")
    end

    it "routes to #update" do
      put("/companions/1").should route_to("companions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/companions/1").should route_to("companions#destroy", :id => "1")
    end

  end
end
