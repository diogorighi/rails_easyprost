# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :companion do
    gender "MyString"
    age 1
    price "9.99"
    boobsize "MyString"
  end
end
